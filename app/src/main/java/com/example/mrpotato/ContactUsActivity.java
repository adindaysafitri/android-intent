package com.example.mrpotato;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

public class ContactUsActivity extends AppCompatActivity {

    //deklarasi elemen
    Button btnCall, btnEmail, btnWhatsapp;
    ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);

        //identifikasi elemen dengan id layout
        btnCall = findViewById(R.id.bt_call);
        btnEmail = findViewById(R.id.bt_email);
        btnWhatsapp = findViewById(R.id.bt_whatsapp);
        back = findViewById(R.id.cu_back);

        //explicit intent - kembali ke halaman MrHeadActivity
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent back = new Intent(ContactUsActivity.this, MrHeadActivity.class);
                startActivity(back);
            }
        });

        //implicit intent - call, whatsapp
        //call
        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            Intent callIntent = new Intent (Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:+6281234567890"));
            startActivity(callIntent);
            }
        });
        //whatsapp
        btnWhatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phoneNumber = "6281132299552";
                Intent whatsappIntent = new Intent (Intent.ACTION_VIEW);
                whatsappIntent.setData(Uri.parse("https://api.whatsapp.com/send?phone="+phoneNumber));
                startActivity(whatsappIntent);
            }
        });
        //explicit intent - navigasi ke laman sendEmailActivity
        btnEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent emailIntent = new Intent (ContactUsActivity.this, SendEmailActivity.class);
                startActivity(emailIntent);
            }
        });





    }
}