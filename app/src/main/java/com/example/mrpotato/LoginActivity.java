package com.example.mrpotato;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    //Deklarasi elemen
    Button btnLogin;
    EditText fEmail;
    EditText fPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //identifikasi elemen dengan id layout
        btnLogin = findViewById(R.id.bt_login);
        fEmail = findViewById(R.id.f_email);
        fPassword = findViewById(R.id.f_password);

        //set listener pada button Login
        btnLogin.setOnClickListener(new View.OnClickListener() {

            //event handler ketika button login di-klik
            @Override
            public void onClick(View v) {
                String email = fEmail.getText().toString();
                String password = fPassword.getText().toString();
                //aturan untuk value email dan password yang dimasukkan
                String emailMhs = "adindaysafitri@gmail.com";
                String nimMhs = "225150701111012";
                if ((email.isEmpty()) || (password.isEmpty())) {
                    Toast.makeText(LoginActivity.this, "Email dan Password tidak boleh kosong", Toast.LENGTH_SHORT).show();
                } else if ((!email.equals(emailMhs)) || (!password.equals(nimMhs))){
                    Toast.makeText(LoginActivity.this, "Email dan Password tidak sesuai", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(LoginActivity.this, "Berhasil Log In", Toast.LENGTH_SHORT).show();

                    //explicit intent - navigasi ke MrHeadActivity serta menyimpan data email dan password yang dimasukkan
                    Intent loginIntent = new Intent(LoginActivity.this, MrHeadActivity.class);
                    loginIntent.putExtra("emailkey", email);
                    loginIntent.putExtra("passwordkey", password);
                    startActivity(loginIntent);
                }
            }
        });
    }
}