package com.example.mrpotato;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class SendEmailActivity extends AppCompatActivity {

    //deklarasi komponen
    ImageView back;
    EditText sm_recipient, sm_subject, sm_message;
    Button btnSend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_email);

        //inisialisasi komponen
        back = findViewById(R.id.sm_back);
        sm_recipient = findViewById(R.id.sm_recipient);
            //set kolom sm_recipient dengan value "filkom@ub.ac.id"
            String emailRecipient = "filkom@ub.ac.id";
            sm_recipient.setText(emailRecipient);
        sm_subject = findViewById(R.id.sm_subject);
        sm_message = findViewById(R.id.sm_message);
        btnSend = findViewById(R.id.bt_send);

        //explicit intent - kembali ke laman ContactUsActivity
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent back = new Intent(SendEmailActivity.this, ContactUsActivity.class);
                startActivity(back);
            }
        });

        //implicit intent - memanggil method sendEmail untuk mengirim email
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String subject = sm_subject.getText().toString();
                String message = sm_message.getText().toString();
                String recipient = "filkom@ub.ac.id";
                sendEmail(recipient, subject, message);
            }
        });
    }

    private void sendEmail(String recipient, String subject, String message) {
        //set penerima/recipient email menjadi "filkom@ub.ac.id"

        Intent sendEmailIntent = new Intent(Intent.ACTION_SEND);
        sendEmailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{recipient});
        sendEmailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        sendEmailIntent.putExtra(Intent.EXTRA_TEXT, message);
        sendEmailIntent.setType("message/rfc822");
        startActivity(Intent.createChooser(sendEmailIntent, "Choose Email Client: "));
    }

}