package com.example.mrpotato;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

public class MrHeadActivity extends AppCompatActivity {

    //deklarasi komponen
    CheckBox c_hair, c_eyebrow, c_beard, c_moustache;
    ImageView hair, eyebrow, beard, moustache;
    TextView key_email, key_password;
    Button contact_us;

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_head);

        //inisialisasi komponen
        key_email = findViewById(R.id.email);
        key_password = findViewById(R.id.password);
        c_eyebrow = findViewById(R.id.c_eyebrow);
        c_hair = findViewById(R.id.c_hair);
        c_beard = findViewById(R.id.c_beard);
        c_moustache = findViewById(R.id.c_moustache);
        eyebrow = findViewById(R.id.eyebrow);
        hair = findViewById(R.id.hair);
        beard = findViewById(R.id.beard);
        moustache = findViewById(R.id.moustache);
        contact_us = findViewById(R.id.bt_contactUs);

        //mengatur visibility komponen dengan checkbox
        //rambut
        c_hair.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    hair.setVisibility(View.INVISIBLE);
                } else hair.setVisibility(View.VISIBLE);
            }
        });
        //alis
        c_eyebrow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    eyebrow.setVisibility(View.INVISIBLE);
                } else eyebrow.setVisibility(View.VISIBLE);
            }
        });
        //kumis
        c_moustache.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    moustache.setVisibility(View.INVISIBLE);
                } else moustache.setVisibility(View.VISIBLE);
            }
        });
        //janggut
        c_beard.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    beard.setVisibility(View.INVISIBLE);
                } else beard.setVisibility(View.VISIBLE);
            }
        });

        //explicit intent - menampilkan data tersimpan pada LoginActivity
        Intent data = getIntent();
        String email = data.getStringExtra("emailkey");
        key_email.setText("Email: " + email);
        String password = data.getStringExtra("passwordkey");
        key_password.setText("NIM: " + password);

        //explicit intent - navigasi ke halaman ContactUsActivity
        contact_us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent contactUs = new Intent(MrHeadActivity.this, ContactUsActivity.class);
                startActivity(contactUs);
            }
        });
    }
}